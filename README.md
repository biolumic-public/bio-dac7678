# Arduino library for TI DAC7678 (12-bit I2C DAC)

This library originated from [https://github.com/deskwizard/Arduino_DAC_ADC_libraries](https://github.com/deskwizard/Arduino_DAC_ADC_libraries) but has
been substantially re-written, primarily for stylistic reasons.

------------------------------------------------------------------------------

## Channel Naming

This library expects you to use the name of the DAC channel mentioned here.

| DAC Channel Numer | DAC Channel Name |
| ----------------- | -----------------|
| 0                 | VOUT_A           |
| 1                 | VOUT_B           |
| ...               |                  |
| 7                 | VOUT_H           |

## Feature support

This library supports the following features of the DAC7678

- Enabling/disabling the internal reference (static mode only)
- Read/Write to DAC input register channel n
- Write to DAC input register channel n, and update all DAC registers (global software LDAC)
- Power up/down DAC
- Write to clear code register
- Write to LDAC register
- Software reset

This library does not support the following features of the DAC7678

- Internal reference flexible mode functions
- Broadcasting update to multiple DACs

## How To Use

Either compile the `.cpp` files into a static library and link into your project, or compile the `.cpp` files with your project. Include `DAC7678.h` in your source code and use the provided API (see `DAC7678.h` for Doxygen-style API definitions).  

## Code Examples

See `examples/`.

## Power Off

The Power off state of the channels have 3 possible states on the DAC7678 and are used as follows:

- HIGHZ  -  DAC channel(s) powered OFF, High-Z mode (High Impedance) **DEFAULT**
- L1K    -  DAC channel(s) powered OFF, 1K pulldown
- L100K  -  DAC channel(s) powered OFF, 100K resistor

## Clear Pin

The settings available for the value sets on all channels when clear pin is brought low are as follows:

- FULL  -  Channels set to 4095  
- MID   -  Channels set to mid-point
- ZERO  -  Channels are set to 0 **DEFAULT**
- NOCLR -  Clear pin is ignored

## LDAC Function

To use the LDAC function, connect the LDAC pin to VCC.

When enabled for a pin, use the `update()` function to update the DAC register without writing to the output. To write to the, the output, use `set()` for the last pin to be written.

If LDAC is not enabled, `update()` act exactly as `set()`

See the DAC7678_sync example (`examples/DAC7678_sync`).

/**
 * 
 * Biolumic Pte Ltd.
 * 21 Dairy Farm Road
 * Palmerston North 4410
 * New Zealand
 * 
 * @file DAC7678.cpp
 * 
 * @author Justin Butler
 * 
 *  Credit goes the orginal author deskwizard
 *  https://github.com/deskwizard/Arduino_DAC_ADC_libraries
 */

#include "DAC7678.h"

const uint8_t CMD_WRITE_BASE_ADDR       = 0x00;
const uint8_t CMD_SELECT                = 0x10;
const uint8_t CMD_IS_LDAC_BASE_ADDR     = 0x30;
const uint8_t CMD_POWER_DOWN            = 0x40;
const uint8_t CMD_CLEAR_CODE            = 0x50;
const uint8_t CMD_LDAC                  = 0x60;
const uint8_t CMD_SOFT_RESET            = 0x70;
const uint8_t CMD_INTREF_RS             = 0x80;

DAC7678::DAC7678(uint8_t _address, TwoWire &wire) :
    m_wire(wire), // Set Wire interface
    m_dac7678_address(_address) // Set DAC address to pass to transmit function
{
    // Do nothing
}

void DAC7678::begin()
{
    m_wire.begin(); // Initialize I2C Bus
    reset();      // Sends Power-On Reset command to DAC
    disable();    // Make sure outputs are powered off
}

DacError DAC7678::reset()
{
    // Issues Reset command (Same as Power-On reset)
    uint8_t lowByte = 0x00;
    uint8_t highByte = 0x00;

    if (transmit(CMD_SOFT_RESET, highByte, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::setVref(DacVref _vref)
{
    // Sets reference mode
    uint8_t lowByte = static_cast<uint8_t>(_vref);
    uint8_t highByte = 0x00;

    if (transmit(CMD_INTREF_RS, highByte, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::setLdac(DacChannel _channel, DacLdacState _state)
{
    // Configure LDAC mode for specified channel
    if (_state == DacLdacState::ENABLE)
    {
        m_LDAC_reg &= ~(1 << static_cast<uint8_t>(_channel));
    }
    else
    {
        m_LDAC_reg |= (1 << static_cast<uint8_t>(_channel));
    }

    uint8_t lowByte = 0x00;
    uint8_t highByte = m_LDAC_reg;

    if (transmit(CMD_LDAC, highByte, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::setLdac(DacLdacState _state)
{
    // Configure LDAC mode for all channels
    m_LDAC_reg = static_cast<uint8_t>(_state);
    
    uint8_t lowByte = 0x00;
    uint8_t highByte = m_LDAC_reg;

    if (transmit(CMD_LDAC, highByte, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::offMode(DacChannel _channel, DacPowerMode _mode)
{
    // Configure off mode for specified channel
    m_off_mode[static_cast<uint8_t>(_channel)] = _mode;

    // If the channel is already off, disable it again so the
    // off mode takes affect immediately
    if (!m_channelState[static_cast<uint8_t>(_channel)])
    {
        DacError retval = disableChannel(static_cast<DacChannel>(_channel));
        
        if (retval != DacError::NO_ERROR)
        {
            return retval;
        }
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::offMode(DacPowerMode _mode)
{
    // Configure off mode for all channels
    for (uint8_t x = 0; x <= 7; x++)
    {
        DacError retval = offMode(static_cast<DacChannel>(x), _mode);

        if (retval != DacError::NO_ERROR)
        {
            return retval;
        }
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::enableChannel(DacChannel _channel)
{
    // Enables the specific DAC output channel
    uint16_t x = 0x20; // Channels are offset +5 bits
    x = x << static_cast<uint8_t>(_channel);
    uint8_t highByte = highByte(x);
    uint8_t lowByte = lowByte(x);

    if (transmit(CMD_POWER_DOWN, highByte, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    m_channelState[static_cast<uint8_t>(_channel)] = true;
    return DacError::NO_ERROR;
}

DacError DAC7678::enable()
{
    // Enables all the channels at once
    uint8_t lowByte = 0xE0;
    uint8_t highByte = 0x1F;

    if (transmit(CMD_POWER_DOWN, highByte, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::disableChannel(DacChannel _channel)
{
    // Disables the specific DAC output channel
    uint16_t x = 0x20 << static_cast<uint8_t>(_channel); // Channels are offset +5 bits
    uint8_t highByte = highByte(x);
    highByte |= static_cast<uint8_t>(m_off_mode[static_cast<uint8_t>(_channel)]);
    uint8_t lowByte = lowByte(x);

    if (transmit(CMD_POWER_DOWN, highByte, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    m_channelState[static_cast<uint8_t>(_channel)] = false;
    return DacError::NO_ERROR;
}


DacError DAC7678::disable()
{
    // Disables all the channels in one go
    // Doing it one by one is required in case of different off modes per channel
    for (uint8_t x = 0; x <= 7; x++)
    {
        DacError retval = disableChannel(static_cast<DacChannel>(x));

        if (retval != DacError::NO_ERROR)
        {
            return retval;
        }
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::set(int16_t _value)
{
    // Sets all DAC channels to specified value
    for (uint8_t x = 0; x <= 7; x++)
    {
        DacError retval = set(static_cast<DacChannel>(x), _value);
        
        if (retval != DacError::NO_ERROR)
        {
            return retval;
        }
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::set(DacChannel _channel, int16_t _value)
{
    // Set specified channel (0-7) to the specified value

    //   Check for out of range values
    if (_value >= 4096 || _value < 0)
    {
        return DacError::INVALID_VALUE;
    }

    // Sets the variables
    uint8_t _command = CMD_IS_LDAC_BASE_ADDR + static_cast<uint8_t>(_channel);

    uint16_t x = _value << 4; // Data is offset +4 bits
    uint8_t highByte = highByte(x);
    uint8_t lowByte = lowByte(x);

    // Send data to DAC
    if (transmit(_command, highByte, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::update(DacChannel _channel, int16_t _value)
{
    // Update specified channel (0-7) to the specified value

    //   Check for out of range values
    if (_value >= 4096 || _value < 0)
    {
        return DacError::INVALID_VALUE;
    }

    // Sets the variables
    uint8_t _command = CMD_WRITE_BASE_ADDR + static_cast<uint8_t>(_channel);

    uint16_t x = _value << 4; // Data is offset +4 bits
    uint8_t highByte = highByte(x);
    uint8_t lowByte = lowByte(x);

    // Send data to DAC
    if (transmit(_command, highByte, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    return DacError::NO_ERROR;
}

DacError DAC7678::clrMode(DacClearPinMode _value)
{
    // Configures the DAC value to output on all channels when CLR pin is brought low
    // Will set the CLR Code register to output either zero, half-range, full range or to ignore the CLR pin

    uint8_t lowByte = static_cast<uint8_t>(_value) << 4;

    // Send data to DAC
    if (transmit(CMD_CLEAR_CODE, 0x00, lowByte))
    {
        return DacError::I2C_ERROR;
    }

    return DacError::NO_ERROR;
}

int16_t DAC7678::readChan(DacChannel _channel)
{
    int16_t reading = 0;

    m_wire.beginTransmission(m_dac7678_address);
    
    // Then send a command byte for the register to be read.
    if (!m_wire.write(static_cast<uint8_t>(_channel)) ||
        m_wire.endTransmission() ||
        m_wire.requestFrom(m_dac7678_address, 2) != 2)
    {
        // I2C error
        return -1;
    }

    if (2 <= m_wire.available())
    {                           // if two bytes were received
        reading = m_wire.read();  // receive high byte
        reading = reading << 8; // shift high byte to be high 8 bits
        reading |= m_wire.read();
        reading = reading >> 4;
    }
    else
    {
        // I2C error
        return -1;
    }

    return reading;
}

int8_t DAC7678::transmit(uint8_t _command, uint8_t _highByte, uint8_t _lowByte)
{
    // Transmit the actual command and high and low bytes to the DAC
    m_wire.beginTransmission(m_dac7678_address);
    
    if (!m_wire.write(_command) ||
        !m_wire.write(_highByte) ||
        !m_wire.write(_lowByte))
    {
        // I2C error
        return -1;
    }
    
    return m_wire.endTransmission();
}
 

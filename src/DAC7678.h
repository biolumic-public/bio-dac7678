/**
 * 
 * Biolumic Pte Ltd.
 * 21 Dairy Farm Road
 * Palmerston North 4410
 * New Zealand
 * 
 * @file DAC7678.h
 * 
 * @author Justin Butler
 * 
 *  Credit goes the orginal author deskwizard
 *  https://github.com/deskwizard/Arduino_DAC_ADC_libraries
 */

#pragma once

#ifndef DAC7678_H
#define DAC7678_H

#include <Arduino.h>
#include <Wire.h>

/**
 * @brief	LDAC options.  Enum values are not arbitrary,
 *          and values must set the right bits via I2C commands.
 * 
 */
enum class DacLdacState : uint8_t
{
    DISABLE = 0x00,
    ENABLE = 0x01
};

/**
 * @brief	VREF options (Regular/StaticMode).  Refer to pages 24/26 of
 *          the data sheet for more details on the Vref modes.  Enum values
 *          are not arbitrary, and values must set the right bits via I2C commands.
 * 
 */
enum class DacVref : uint8_t
{
    EXT = 0x00, // External VREF
    INT = 0x10  // Internal VREF
};

/**
 * @brief	DAC power mode.  Enum values are not arbitrary,
 *          and values must set the right bits via I2C commands.
 * 
 */
enum class DacPowerMode : uint8_t
{
    OFF = 0x00,
    ON = 0x01,
    L1K = 0x20,   // DAC channel(s) powered OFF, 1K pulldown
    L100K = 0x40, // DAC channel(s) powered OFF, 100K resistor
    HIGHZ = 0x60  // DAC channel(s) powered OFF, High-Z mode (High Impedance) ** DEFAULT **
};

/**
 * @brief   Defines what values can be set on all channels when the clear pin is pulled low:
 * 
 */
enum class DacClearPinMode : uint8_t
{
    ZERO = 0, // Channels are set to 0 **  DEFAULT **
    MID = 1,  // Channels set to mid-point
    FULL = 2, // Channels set to 4095
    NOCLR = 3 // Clear pin is ignored
};

/**
 * @brief	Channel number to channel name mapping.
 * 
 */
enum class DacChannel : uint8_t
{
    VOUT_A = 0,
    VOUT_B = 1,
    VOUT_C = 2,
    VOUT_D = 3,
    VOUT_E = 4,
    VOUT_F = 5,
    VOUT_G = 6,
    VOUT_H = 7
};

enum class DacError : uint8_t
{
    NO_ERROR = 0,   // Must equal 0 so it can be used in if statements.
    I2C_ERROR,
    INVALID_VALUE
};

class DAC7678
{

public:
    TwoWire &m_wire;
    const uint8_t m_dac7678_address;

    /**
     * @brief   Construct a new DAC7678 object
     * 
     * @param _address  I2C address of the DAC
     * @param wire      Wire interface to use (Defaults to Wire)
     */
    DAC7678(uint8_t _address, TwoWire &wire = Wire);

    /**
	 * @brief 	Initialize the DAC.
	 * 
	 */
    void begin();

    /**
	 * @brief 	Send Reset command to DAC.
	 * 			Same as Power-On reset, called by begin.
     * @return              Returns 0 on success.
	 * 
	 */
    DacError reset();

    /**
	 * @brief 	Set Vref mode.
	 * 
	 * @param _vref
     * @return              Returns 0 on success.
	 */
    DacError setVref(DacVref _vref); 
    
    /**
     * @brief   Configure LDAC for a single channel.
     * 
     * @param _channel      DAC channel.
     * @param _state        LDAC state.
     * @return              Returns 0 on success.
     */
    DacError setLdac(DacChannel _channel, DacLdacState _state);

    /**
     * @brief   Configure LDAC for all channels.
     * 
     * @param _state        LDAC state.
     * @return              Returns 0 on success.
     */
    DacError setLdac(DacLdacState _state);

    /**
	 * @brief 	Configure Power down mode for a single channel.
	 * 
	 * @param _channel 		DAC channel.
	 * @param _mode 		Dac channel power mode.
     * @return              Returns 0 on success.
	 */
    DacError offMode(DacChannel _channel, DacPowerMode _mode);

    /**
	 * @brief 	Configure power down mode for all channels.
	 * 
	 * @param _mode 		Dac channel power mode.
     * @return              Returns 0 on success.
	 */
    DacError offMode(DacPowerMode _mode);

    /**
	 * @brief 	Enable a single channel.
	 * 
	 * @param _channel 		Dac channel to enable.
     * @return              Returns 0 on success.
	 */
    DacError enableChannel(DacChannel _channel);

    /**
	 * @brief 	Enable all channels.
	 *
     * @return              Returns 0 on success. 
	 */
    DacError enable();

    /**
	 * @brief 	Disable a single channel.
	 * 
	 * @param _channel 		Dac channel to disable.
     * @return              Returns 0 on success.
	 */
    DacError disableChannel(DacChannel _channel);

    /**
	 * @brief 	Disable all channels.
	 *
     * @return              Returns 0 on success. 
	 */
    DacError disable();

    /**
	 * @brief 	Set all channels Vout.
	 * 
	 * @param _value 		Vout value (0 - 4095).
     * @return              Returns 0 on success.
	 */
    DacError set(int16_t _value);

    /**
	 * @brief 	Set single channel Vout.
	 * 
	 * @param channel 		Dac channel.
	 * @param _value 		Vout value (0 - 4095).
     * @return              Returns 0 on success.
	 */
    DacError set(DacChannel channel, int16_t _value);

    /**
	 * @brief 	When LDAC is enabled for a pin, use this function to update
	 * 			the DAC register without writing to the output. To write to
	 * 			the output, use set() for the last pin to be written. 
	 *			If LDAC is not enabled, `update()` acts exactly as set().
	 *			To use the LDAC function, connect the LDAC pin to VCC.
	 * 
	 * @param _channel 		Dac channel.
	 * @param _value 		Vout value (0 - 4095).
     * @return              Returns 0 on success.
	 */
    DacError update(DacChannel _channel, int16_t _value);

    /**
	 * @brief 	Configure clear pin.
	 * 
	 * @param _value 		Clear pin mode.
     * @return              Returns 0 on success.
	 */
    DacError clrMode(DacClearPinMode _value);

    /**
	 * @brief 	Read a DAC channel
	 * 
	 * @param _channel 		DAC channel to read.
	 * @return      		Dac channel value (0-4095).
     *                      A value of -1 indicates an error occurred reading from the DAC.
	 */
    int16_t readChan(DacChannel _channel);

private:
    DacPowerMode m_off_mode[8] = {DacPowerMode::HIGHZ};  // Set default off mode to high Z
    bool m_channelState[8] = {false};  // Track the channel states
    uint8_t m_LDAC_reg = static_cast<uint8_t>(DacLdacState::DISABLE);
    
    /**
     * @brief   Send I2C command to DAC.
     * 
     * @param _command      Command to send to the DAC
     * @param _highByte     High byte of command payload
     * @param _lowByte      Low byte of command payload
     * @return              0 : Success
     *                      1 : Data too long
     *                      2 : NACK on transmit of address
     *                      3 : NACK on transmit of data
     *                      4 : Other error
     */
    int8_t transmit(uint8_t _command, uint8_t _highByte, uint8_t _lowByte);
};

#endif

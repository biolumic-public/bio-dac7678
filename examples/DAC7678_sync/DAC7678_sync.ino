/*
  Example sketch for the TI DAC7678 12bits I²C DAC Library
  Demonstrate selective batch updating of DAC channels

  ------------------------------------------------------------------------------

  Chip Wiring

  Connect GND pin to Ground
  Connect AVDD pin to 5v
  Connect ADDR0 to ground
  Connect LDAC pin to 5v
  Connect SDA pin to Arduino SDA (Analog 4 on Uno)
  Connect SCL pin to Arduino SCL (Analog 5 on Uno)
  Connect CLR pin to 5v

*/

#include <DAC7678.h> // Include DAC7678.h for TI DAC7678 functions

DAC7678 dac(0x48, Wire); // Set the Wire interface and DAC address to 0x48 (72 in decimal)

void setup()
{

    Serial.begin(9600);
    Serial.println("Sketch started");
    Serial.println();

    dac.begin();                                          // Initialize the DAC
    dac.setVref(DacVref::INT);                            // Configure for internal voltage reference
    dac.setLdac(DacChannel::VOUT_C, DacLdacState::DISABLE); // Disable LDAC for channel 2
    delay(2000);

    dac.enable();

    Serial.println("Setting channel 1 to full scale");
    dac.set(DacChannel::VOUT_B, 4095);
    delay(2000);

    dac.update(DacChannel::VOUT_A, 4095); // Won't write since LDAC is enabled on channel 0
    Serial.println("Setting channel 2 to full scale");
    dac.update(DacChannel::VOUT_C, 4095); // Will write since LDAC is disabled on channel 2
    delay(2000);

    Serial.println("Setting channel 0 to full scale & channel 1 to 0");
    dac.set(DacChannel::VOUT_B, 0); // Writes channel 0 and channel 2
}

void loop() {}
